from flask import Flask
import logging
from logging.config import dictConfig

dictConfig(
    {
        "version": 1,
        "formatters": {
            "gunicorn_json": {
                "format": '{"AppName": "%(name)s", "logLevel": "%(levelname)s", "Timestamp": "%(created)f", "Class_Name":"%(module)s", "Method_name": "%(funcName)s", "process_id":%(process)d, "message": "%(message)s"}'
            }
        },
        "handlers": {
            "console": {
                "class": "logging.StreamHandler",
                "stream": "ext://sys.stdout",
                "formatter": "gunicorn_json",
            }
        },
        "loggers": {
            "gunicorn.access": {
                "level": "INFO",
                "handlers": [
                    "console"
                ]
            }
        }
    }
)


def create_app():
    app = Flask(__name__)

    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)

    with app.app_context():
        from . import views  # noqa: E402,F401
        from . import apis  # noqa: E402,F401
    return app
